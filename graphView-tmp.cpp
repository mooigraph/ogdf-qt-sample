#include <QRectF>
#include <QGraphicsScene>
#include <QBrush>
#include <QPen>
#include <QPointF>
#include <QPainterPath>
#include <QGraphicsEllipseItem>
#include <QGraphicsSimpleTextItem>
#include <QString>
#include <QLineF>
#include <QPolygonF>
#include <QDebug>

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/planarity/PlanarizationLayout.h>
#include <ogdf/fileformats/GraphIO.h>

#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/SugiyamaLayout.h>

#include "graphView.h"

using namespace ogdf;

//const GraphIO::SVGSettings &m_settings;

GraphView::GraphView(QWidget* parent) :
    QGraphicsView(parent)
{
    graph_ = new Graph();

//    GA_ = new GraphAttributes(*graph_,
//        GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics);

	GA_ = new GraphAttributes(*graph_,
	  GraphAttributes::nodeGraphics |
	  GraphAttributes::edgeGraphics |
	  GraphAttributes::nodeLabel |
	  GraphAttributes::edgeStyle |
	  GraphAttributes::nodeStyle |
	  GraphAttributes::nodeTemplate);

    scene_ = new QGraphicsScene(this);
    scene_->setBackgroundBrush(QBrush(QColor("white")));

    setScene(scene_);

    setRenderHint(QPainter::Antialiasing);

//randomGraph(graph_,10,35);

}

node GraphView::addNode(const QSize& size)
{
    node newNode = graph_->newNode();

    GA_->width(newNode) = size.width();
    GA_->height(newNode) = size.height();

    nodeList_.push_back(newNode);

    return newNode;
}

 void GraphView::addEdge(const ogdf::node& source,
         const ogdf::node& target)
{
    edge e = graph_->newEdge(source, target);

    edgeList_.push_back(e);
}

void GraphView::layout()
{

#if 0
    PlanarizationLayout layout;
    layout.call(*GA_);
#endif

#if 1
	SugiyamaLayout SL;

	SL.setRanking(new OptimalRanking);
	SL.setCrossMin(new MedianHeuristic);

	OptimalHierarchyLayout *ohl = new OptimalHierarchyLayout;
	ohl->layerDistance(20.0);
	ohl->nodeDistance(80.0);
	ohl->weightBalancing(0.5);
	SL.setLayout(ohl);

	SL.call(*GA_);
#endif

	GraphIO::write(*GA_, "output-example.svg", GraphIO::drawSVG);


    //
    // Draw the edges
    //
   GraphView::drawEdges();

    //
    // Draw the nodes
    // 
    for ( int idx=0; idx<nodeList_.size(); ++idx)
    {
        node n = nodeList_[idx];

        double x = GA_->x(n);
        double y = GA_->y(n);
        double w = GA_->width(n);
        double h = GA_->height(n);

        QRectF rect(x-w/2, y-h/2, w, h);

        QGraphicsRectItem* ri = 
            scene_->addRect(rect, QPen(QColor("green")));


        QString ls = QString("N%1").arg(idx+1);

        QGraphicsSimpleTextItem* newLabel =
            new QGraphicsSimpleTextItem(ls);
        QRectF textRect = newLabel->boundingRect();
        double newx = (w - textRect.width())/2;
        double newy = (h - textRect.height())/2;

        newLabel->setBrush(QColor("red"));
        newLabel->setParentItem(ri);
        newLabel->setPos(x-w/2+newx, y-h/2+newy);
    }

#if 0



    for ( int idx=0; idx<edgeList_.size(); ++idx)
    {
        edge e = edgeList_[idx];
        DPolyline& points = GA_->bends(e);

        List<DPoint>::const_iterator iter = points.begin();
        if ( iter != points.end() )
        {
            QPointF startPoint((*iter).m_x, (*iter).m_y);
            QPainterPath path(startPoint);

            scene_->addPath(path, QPen(QColor("green")));

            for ( ; iter != points.end(); ++iter )
            {
                DPoint dp = *iter;
                QPointF nextPoint(dp.m_x, dp.m_y);
                path.lineTo(nextPoint);
            }

            scene_->addPath(path, QPen(QColor("green")));

            List<DPoint>::iterator arrowStartPoint =
                    points.get(points.size() - 2);
            List<DPoint>::iterator arrowEndPoint =
                points.get(points.size() - 1);

//            QPolygonF arrow = createArrow(
//                    QPointF((*arrowStartPoint).m_x, (*arrowStartPoint).m_y),
//                    QPointF((*arrowEndPoint).m_x, (*arrowEndPoint).m_y));
//            path.addPolygon(arrow);

#if 0
            drawArrow(
                QPointF((*arrowStartPoint).m_x, (*arrowStartPoint).m_y),
                QPointF((*arrowEndPoint).m_x, (*arrowEndPoint).m_y),
                QColor("green")
            );
#endif
            //scene_->addPath(path, QPen(QColor("green")));

#if 0
            QRectF epr(
               (*arrowEndPoint).m_x-3,
               (*arrowEndPoint).m_y-3,
               6, 6);
            scene_->addEllipse(epr, QPen(QColor("green")),
                    QBrush(QColor("green")));
#endif


        }
    }
#endif




}

 QPolygonF GraphView::
 createArrow(const QPointF& start, const QPointF& end)
{
    qreal Pi = 3.14;
    qreal arrowSize = 10;

    QPolygonF arrowHead;

    QLineF line(end, start);

    double angle = ::acos(line.dx() / line.length());

    if ( line.dy() >= 0 )
        angle = (Pi * 2) - angle;

    QPointF arrowP1 = line.p1() + QPointF(sin(angle+Pi/3)*arrowSize,
            cos(angle+Pi/3)*arrowSize);
    QPointF arrowP2 = line.p1() + QPointF(sin(angle+Pi-Pi/3)*arrowSize,
            cos(angle+Pi-Pi/3)*arrowSize);


    arrowHead.clear();
    arrowHead << line.p1() << arrowP1 << arrowP2;

    return arrowHead;
}

void GraphView::
drawArrow(const QPointF& start, const QPointF& end, const QColor& color)
{
    qreal Pi = 3.14;
    qreal arrowSize = 10;

    QPolygonF arrowHead;

    QLineF line(end, start);

    double angle = ::acos(line.dx() / line.length());

    if ( line.dy() >= 0 )
        angle = (Pi * 2) - angle;

    QPointF arrowP1 = line.p1() + QPointF(sin(angle+Pi/3)*arrowSize,
            cos(angle+Pi/3)*arrowSize);
    QPointF arrowP2 = line.p1() + QPointF(sin(angle+Pi-Pi/3)*arrowSize,
            cos(angle+Pi-Pi/3)*arrowSize);


    arrowHead.clear();
    arrowHead << line.p1() << arrowP1 << arrowP2;

    scene_->addPolygon(arrowHead, QPen(color), QBrush(color));

    scene_->addLine(line, QPen(color));
}

///// edges /////


// draw all edges
void GraphView::drawEdges(void)
{

    // only if edge graphics
    if (GA_->has(GraphAttributes::edgeGraphics))
    {
	// scan all edges
	for(edge e : graph_->edges)
	{
		drawEdge(e);
	}
    }

	return;
}

// draw one edge
void GraphView::drawEdge(edge e)
{
	bool drawSourceArrow = false;
	bool drawTargetArrow = false;

	// draw arrows if G is directed or if arrow types are defined for the edge
	// enum class EdgeArrow {
	// None, //!< no edge arrows
	// Last, //!< edge arrow at target node of the edge
	// First, //!< edge arrow at source node of the edge
	// Both, //!< edge arrow at target and source node of the edge
	// Undefined
	// };
	if (GA_->has(GraphAttributes::edgeArrow)) {
		switch (GA_->arrowType(e))
		{
		case EdgeArrow::Undefined:
			drawTargetArrow = GA_->directed();
			break;
		case EdgeArrow::Last:
			drawTargetArrow = true;
			break;
		case EdgeArrow::Both:
			drawTargetArrow = true;
			OGDF_CASE_FALLTHROUGH;
		case EdgeArrow::First:
			drawSourceArrow = true;
			break;
		case EdgeArrow::None:
		default: // default should not happen
			// don't draw any arrows
			drawSourceArrow = false;
			drawTargetArrow = false;
			break;
		}
	}



	bool drawLabel = GA_->has(GraphAttributes::edgeLabel) && !GA_->label(e).empty();


#if 0
	if(drawLabel) {
		label = xmlNode.append_child("text");
		label.append_attribute("text-anchor") = "middle";
		label.append_attribute("dominant-baseline") = "middle";
		label.append_attribute("font-family") = m_settings.fontFamily().c_str();
		label.append_attribute("font-size") = m_settings.fontSize();
		label.append_attribute("fill") = m_settings.fontColor().c_str();
		label.text() = m_attr.label(e).c_str();
	}
#endif


	DPolyline path = GA_->bends(e);

	node s = e->source();
	node t = e->target();

	path.pushFront(DPoint(GA_->x(s), GA_->y(s)));
	path.pushBack(DPoint(GA_->x(t), GA_->y(t)));

	bool drawSegment = false;
	bool finished = false;


	List<DPoint> points;

	std::cout<< "--egde" << points.size() << std::endl;

	for(ListConstIterator<DPoint> it = path.begin(); it.succ().valid() && !finished; it++) {
		DPoint p1 = *it;
		DPoint p2 = *(it.succ());

	std::cout<< "--here" << points.size() << std::endl;

		// leaving segment at source node ?
		if(isCoveredBy(p1, e, s) && !isCoveredBy(p2, e, s)) {

#if 0
			// optional arrow at source node
			if(!drawSegment && drawSourceArrow) {
				drawArrowHead(xmlNode, p2, p1, s, e);
			}
#endif

			drawSegment = true;
		}

		// entering segment at target node ?
		if(!isCoveredBy(p1, e, t) && isCoveredBy(p2, e, t)) {
			finished = true;

#if 0
			// optional arrow at tarfet node
			if(drawTargetArrow) {
				drawArrowHead(xmlNode, p1, p2, t, e);
			}
#endif

		}

#if 0
		if(drawSegment && drawLabel) {
			label.append_attribute("x") = (p1.m_x + p2.m_x) / 2;
			label.append_attribute("y") = (p1.m_y + p2.m_y) / 2;

			drawLabel = false;
		}
#endif

		if(drawSegment) {
			points.pushBack(p1);
		}

		if(finished) {
			points.pushBack(p2);
		}
	}


	if(points.size() < 2) {
		GraphIO::logger.lout() << "Could not draw edge since nodes are overlapping: " << e << std::endl;
	} else {
		drawCurve(e, points);
	}


	return;
}

// draw line segments of edge
void GraphView::drawCurve(edge e, List<DPoint> &points)
{
	OGDF_ASSERT(points.size() >= 2);

	std::cout<< "--" << points.size() << std::endl;

	if(points.size() == 2) {
		const DPoint p1 = points.popFrontRet();
		const DPoint p2 = points.popFrontRet();

		drawLine(p1, p2);
	}else {
//		if(GraphIO::m_settings.curviness() == 0) {
			drawLines(points);
//		} else if(m_settings.bezierInterpolation()) {
//			drawBezierPath(points);
//		} else {
//			drawRoundPath(points);
//		}
	}

#if 0
	line.append_attribute("fill") = "none";
	line.append_attribute("d") = ss.str().c_str();
	appendLineStyle(line, e);
#endif

	return;
}

//
bool GraphView::isCoveredBy(const DPoint &point, edge e, node v)
{
	double arrowSize = getArrowSize(e, v);

	return point.m_x >= GA_->x(v) - GA_->width(v)/2 - arrowSize
	    && point.m_x <= GA_->x(v) + GA_->width(v)/2 + arrowSize
	    && point.m_y >= GA_->y(v) - GA_->height(v)/2 - arrowSize
	    && point.m_y <= GA_->y(v) + GA_->height(v)/2 + arrowSize;
}

//
double GraphView::getArrowSize(edge e, node v)
{
	double result = 0;

	if(GA_->has(GraphAttributes::edgeArrow) || GA_->directed()) {
		const double minSize = (GA_->has(GraphAttributes::edgeStyle) ? GA_->strokeWidth(e) : 1) * 3;
		node w = e->opposite(v);
		result = std::max(minSize, (GA_->width(v) + GA_->height(v) + GA_->width(w) + GA_->height(w)) / 16.0);
	}

	return result;
}

//
void GraphView::drawLine(const DPoint &p1, const DPoint &p2)
{

//	std::cout<< "--" << p1.m_x << "," p1.m_y << " "<<p2.m_x << "," << p2.m_y << std::endl;

//	std::cout << " M" << p1.m_x << "," << p1.m_y << " L" << p2.m_x << "," << p2.m_y << std::endl;

            scene_->addLine(p1.m_x,p1.m_y,p2.m_x,p2.m_y,QPen(QColor("black")) );

    return;
}

void GraphView::drawLines(List<DPoint> &points) {
	while(points.size() > 1) {
		DPoint p = points.popFrontRet();
		drawLine(p, points.front());
	}
	return;
}

//// nodes ////