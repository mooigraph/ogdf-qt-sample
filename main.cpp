/* these files are GNU GPL version 3 libre software */

#include <QApplication>

#include "mainWindow.h"

int
main(int argc, char** argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    app.exec();
}
